From 1b790db193a865cee5cb964b57e6893c8d6b6fd5 Mon Sep 17 00:00:00 2001
From: Geert Uytterhoeven <geert@linux-m68k.org>
Date: Mon, 19 Dec 2016 18:29:23 +0100
Subject: [PATCH] gpio: Move freeing of GPIO hogs before numbing of the device

commit 5018ada69a04c8ac21d74bd682fceb8e42dc0f96 upstream.

When removing a gpiochip that uses GPIO hogging (e.g. by unloading the
chip's DT overlay), a warning is printed:

    gpio gpiochip8: REMOVING GPIOCHIP WITH GPIOS STILL REQUESTED

This happens because gpiochip_free_hogs() is called after the gdev->chip
pointer is reset to NULL. Hence __gpiod_free() cannot determine the
chip in use, and cannot clear flags nor call the optional chip-specific
.free() callback.

Move the call to gpiochip_free_hogs() up to fix this.

Cc: stable@vger.kernel.org
Fixes: ff2b135922992756 ("gpio: make the gpiochip a real device")
Signed-off-by: Geert Uytterhoeven <geert@linux-m68k.org>
Signed-off-by: Linus Walleij <linus.walleij@linaro.org>
Signed-off-by: Paul Gortmaker <paul.gortmaker@windriver.com>

diff --git a/drivers/gpio/gpiolib.c b/drivers/gpio/gpiolib.c
index a8eea8a69da3..52cfe21eb642 100644
--- a/drivers/gpio/gpiolib.c
+++ b/drivers/gpio/gpiolib.c
@@ -1310,12 +1310,12 @@ void gpiochip_remove(struct gpio_chip *chip)
 
 	/* FIXME: should the legacy sysfs handling be moved to gpio_device? */
 	gpiochip_sysfs_unregister(gdev);
+	gpiochip_free_hogs(chip);
 	/* Numb the device, cancelling all outstanding operations */
 	gdev->chip = NULL;
 	gpiochip_irqchip_remove(chip);
 	acpi_gpiochip_remove(chip);
 	gpiochip_remove_pin_ranges(chip);
-	gpiochip_free_hogs(chip);
 	of_gpiochip_remove(chip);
 	/*
 	 * We accept no more calls into the driver from this point, so
-- 
2.10.1

