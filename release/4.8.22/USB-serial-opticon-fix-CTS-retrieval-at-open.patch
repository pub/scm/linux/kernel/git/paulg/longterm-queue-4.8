From c6034912bf14421972722f291662b31ca87df2cf Mon Sep 17 00:00:00 2001
From: Johan Hovold <johan@kernel.org>
Date: Fri, 13 Jan 2017 13:21:08 +0100
Subject: [PATCH] USB: serial: opticon: fix CTS retrieval at open

commit 2eee05020a0e7ee7c04422cbacdb07859e45dce6 upstream.

The opticon driver used a control request at open to trigger a CTS
status notification to be sent over the bulk-in pipe. When the driver
was converted to using the generic read implementation, an inverted test
prevented this request from being sent, something which could lead to
TIOCMGET reporting an incorrect CTS state.

Reported-by: Dan Carpenter <dan.carpenter@oracle.com>
Fixes: 7a6ee2b02751 ("USB: opticon: switch to generic read
implementation")
Cc: stable <stable@vger.kernel.org>
Reviewed-by: Greg Kroah-Hartman <gregkh@linuxfoundation.org>
Signed-off-by: Johan Hovold <johan@kernel.org>
Signed-off-by: Paul Gortmaker <paul.gortmaker@windriver.com>

diff --git a/drivers/usb/serial/opticon.c b/drivers/usb/serial/opticon.c
index 4b7bfb394a32..64bf258e7e00 100644
--- a/drivers/usb/serial/opticon.c
+++ b/drivers/usb/serial/opticon.c
@@ -142,7 +142,7 @@ static int opticon_open(struct tty_struct *tty, struct usb_serial_port *port)
 	usb_clear_halt(port->serial->dev, port->read_urb->pipe);
 
 	res = usb_serial_generic_open(tty, port);
-	if (!res)
+	if (res)
 		return res;
 
 	/* Request CTS line state, sometimes during opening the current
-- 
2.12.0

