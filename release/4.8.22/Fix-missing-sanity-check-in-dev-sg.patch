From cf8364f162c53020b0ea09987e0ab308c7c5a5da Mon Sep 17 00:00:00 2001
From: Al Viro <viro@ZenIV.linux.org.uk>
Date: Sun, 19 Feb 2017 07:15:27 +0000
Subject: [PATCH] Fix missing sanity check in /dev/sg

commit 137d01df511b3afe1f05499aea05f3bafc0fb221 upstream.

What happens is that a write to /dev/sg is given a request with non-zero
->iovec_count combined with zero ->dxfer_len.  Or with ->dxferp pointing
to an array full of empty iovecs.

Having write permission to /dev/sg shouldn't be equivalent to the
ability to trigger BUG_ON() while holding spinlocks...

Found by Dmitry Vyukov and syzkaller.

[ The BUG_ON() got changed to a WARN_ON_ONCE(), but this fixes the
  underlying issue.  - Linus ]

Signed-off-by: Al Viro <viro@zeniv.linux.org.uk>
Reported-by: Dmitry Vyukov <dvyukov@google.com>
Reviewed-by: Christoph Hellwig <hch@lst.de>
Cc: stable@vger.kernel.org
Signed-off-by: Linus Torvalds <torvalds@linux-foundation.org>
Signed-off-by: Paul Gortmaker <paul.gortmaker@windriver.com>

diff --git a/drivers/scsi/sg.c b/drivers/scsi/sg.c
index a1c29b0afb22..dedcff9cabb5 100644
--- a/drivers/scsi/sg.c
+++ b/drivers/scsi/sg.c
@@ -1763,6 +1763,10 @@ sg_start_req(Sg_request *srp, unsigned char *cmd)
 			return res;
 
 		iov_iter_truncate(&i, hp->dxfer_len);
+		if (!iov_iter_count(&i)) {
+			kfree(iov);
+			return -EINVAL;
+		}
 
 		res = blk_rq_map_user_iov(q, rq, md, &i, GFP_ATOMIC);
 		kfree(iov);
-- 
2.12.0

