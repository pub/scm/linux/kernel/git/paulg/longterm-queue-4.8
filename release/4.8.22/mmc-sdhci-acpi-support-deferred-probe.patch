From e0c927c97bb139029cd6a8fd644757395596f3f3 Mon Sep 17 00:00:00 2001
From: Zhang Rui <rui.zhang@intel.com>
Date: Wed, 18 Jan 2017 17:46:18 +0800
Subject: [PATCH] mmc: sdhci-acpi: support deferred probe

commit e28d6f048799acb0014491e6b74e580d84bd7916 upstream.

With commit 67bf5156edc4 ("gpio / ACPI: fix returned error from
acpi_dev_gpio_irq_get()"), mmc_gpiod_request_cd() returns -EPROBE_DEFER if
GPIO is not ready when sdhci-acpi driver is probed, and sdhci-acpi driver
should be probed again later in this case.

This fixes an order issue when both GPIO and sdhci-acpi drivers are built
as modules.

CC: stable@vger.kernel.org # v4.9
Bugzilla: https://bugzilla.kernel.org/show_bug.cgi?id=177101
Tested-by: Jonas Aaberg <cja@gmx.net>
Signed-off-by: Zhang Rui <rui.zhang@intel.com>
Acked-by: Adrian Hunter <adrian.hunter@intel.com>
Signed-off-by: Ulf Hansson <ulf.hansson@linaro.org>
Signed-off-by: Paul Gortmaker <paul.gortmaker@windriver.com>

diff --git a/drivers/mmc/host/sdhci-acpi.c b/drivers/mmc/host/sdhci-acpi.c
index e19f23aca2e9..ecdf472f1e10 100644
--- a/drivers/mmc/host/sdhci-acpi.c
+++ b/drivers/mmc/host/sdhci-acpi.c
@@ -466,7 +466,10 @@ static int sdhci_acpi_probe(struct platform_device *pdev)
 	if (sdhci_acpi_flag(c, SDHCI_ACPI_SD_CD)) {
 		bool v = sdhci_acpi_flag(c, SDHCI_ACPI_SD_CD_OVERRIDE_LEVEL);
 
-		if (mmc_gpiod_request_cd(host->mmc, NULL, 0, v, 0, NULL)) {
+		err = mmc_gpiod_request_cd(host->mmc, NULL, 0, v, 0, NULL);
+		if (err) {
+			if (err == -EPROBE_DEFER)
+				goto err_free;
 			dev_warn(dev, "failed to setup card detect gpio\n");
 			c->use_runtime_pm = false;
 		}
-- 
2.12.0

