From d49d7f0a2ca0df8205c1ec2e7bd552cf345bea8a Mon Sep 17 00:00:00 2001
From: Johan Hovold <johan@kernel.org>
Date: Thu, 2 Feb 2017 17:38:35 +0100
Subject: [PATCH] USB: serial: ftdi_sio: fix line-status over-reporting

commit a6bb1e17a39818b01b55d8e6238b4b5f06d55038 upstream.

FTDI devices use a receive latency timer to periodically empty the
receive buffer and report modem and line status (also when the buffer is
empty).

When a break or error condition is detected the corresponding status
flags will be set on a packet with nonzero data payload and the flags
are not updated until the break is over or further characters are
received.

In order to avoid over-reporting break and error conditions, these flags
must therefore only be processed for packets with payload.

This specifically fixes the case where after an overrun, the error
condition is continuously reported and NULL-characters inserted until
further data is received.

Reported-by: Michael Walle <michael@walle.cc>
Fixes: 72fda3ca6fc1 ("USB: serial: ftd_sio: implement sysrq handling on
break")
Fixes: 166ceb690750 ("USB: ftdi_sio: clean up line-status handling")
Cc: stable <stable@vger.kernel.org>	# v2.6.35
Signed-off-by: Johan Hovold <johan@kernel.org>
Signed-off-by: Paul Gortmaker <paul.gortmaker@windriver.com>

diff --git a/drivers/usb/serial/ftdi_sio.c b/drivers/usb/serial/ftdi_sio.c
index c990af07086f..99a0a5f1b400 100644
--- a/drivers/usb/serial/ftdi_sio.c
+++ b/drivers/usb/serial/ftdi_sio.c
@@ -2070,6 +2070,20 @@ static int ftdi_process_packet(struct usb_serial_port *port,
 		priv->prev_status = status;
 	}
 
+	/* save if the transmitter is empty or not */
+	if (packet[1] & FTDI_RS_TEMT)
+		priv->transmit_empty = 1;
+	else
+		priv->transmit_empty = 0;
+
+	len -= 2;
+	if (!len)
+		return 0;	/* status only */
+
+	/*
+	 * Break and error status must only be processed for packets with
+	 * data payload to avoid over-reporting.
+	 */
 	flag = TTY_NORMAL;
 	if (packet[1] & FTDI_RS_ERR_MASK) {
 		/* Break takes precedence over parity, which takes precedence
@@ -2092,15 +2106,6 @@ static int ftdi_process_packet(struct usb_serial_port *port,
 		}
 	}
 
-	/* save if the transmitter is empty or not */
-	if (packet[1] & FTDI_RS_TEMT)
-		priv->transmit_empty = 1;
-	else
-		priv->transmit_empty = 0;
-
-	len -= 2;
-	if (!len)
-		return 0;	/* status only */
 	port->icount.rx += len;
 	ch = packet + 2;
 
-- 
2.12.0

