From c804779b366d9fb12bb83c7a92bb030a1f89b375 Mon Sep 17 00:00:00 2001
From: Kees Cook <keescook@chromium.org>
Date: Mon, 6 Mar 2017 12:42:12 -0800
Subject: [PATCH] pstore: Shut down worker when unregistering

commit 6330d5534786d5315d56d558aa6d20740f97d80a upstream.

When built as a module and running with update_ms >= 0, pstore will Oops
during module unload since the work timer is still running. This makes sure
the worker is stopped before unloading.

Signed-off-by: Kees Cook <keescook@chromium.org>
Cc: stable@vger.kernel.org
Signed-off-by: Paul Gortmaker <paul.gortmaker@windriver.com>

diff --git a/fs/pstore/platform.c b/fs/pstore/platform.c
index 16ecca5b72d8..0872b8861d99 100644
--- a/fs/pstore/platform.c
+++ b/fs/pstore/platform.c
@@ -667,6 +667,7 @@ int pstore_register(struct pstore_info *psi)
 		pstore_register_pmsg();
 	}
 
+	/* Start watching for new records, if desired. */
 	if (pstore_update_ms >= 0) {
 		pstore_timer.expires = jiffies +
 			msecs_to_jiffies(pstore_update_ms);
@@ -689,6 +690,11 @@ EXPORT_SYMBOL_GPL(pstore_register);
 
 void pstore_unregister(struct pstore_info *psi)
 {
+	/* Stop timer and make sure all work has finished. */
+	pstore_update_ms = -1;
+	del_timer_sync(&pstore_timer);
+	flush_work(&pstore_work);
+
 	if ((psi->flags & PSTORE_FLAGS_FRAGILE) == 0) {
 		pstore_unregister_pmsg();
 		pstore_unregister_ftrace();
@@ -786,7 +792,9 @@ static void pstore_timefunc(unsigned long dummy)
 		schedule_work(&pstore_work);
 	}
 
-	mod_timer(&pstore_timer, jiffies + msecs_to_jiffies(pstore_update_ms));
+	if (pstore_update_ms >= 0)
+		mod_timer(&pstore_timer,
+			  jiffies + msecs_to_jiffies(pstore_update_ms));
 }
 
 module_param(backend, charp, 0444);
-- 
2.12.0

