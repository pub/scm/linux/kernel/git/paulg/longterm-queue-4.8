From 5bfde747c8183d3b73f90ffbd4a2ebdf97da02ad Mon Sep 17 00:00:00 2001
From: Jin Qian <jinqian@google.com>
Date: Tue, 25 Apr 2017 16:28:48 -0700
Subject: [PATCH] f2fs: sanity check segment count

commit b9dd46188edc2f0d1f37328637860bb65a771124 upstream.

F2FS uses 4 bytes to represent block address. As a result, supported
size of disk is 16 TB and it equals to 16 * 1024 * 1024 / 2 segments.

Signed-off-by: Jin Qian <jinqian@google.com>
Signed-off-by: Jaegeuk Kim <jaegeuk@kernel.org>
Signed-off-by: Paul Gortmaker <paul.gortmaker@windriver.com>

diff --git a/fs/f2fs/super.c b/fs/f2fs/super.c
index a3f558dc0bd5..efa18d04791d 100644
--- a/fs/f2fs/super.c
+++ b/fs/f2fs/super.c
@@ -1399,6 +1399,13 @@ static int sanity_check_raw_super(struct f2fs_sb_info *sbi,
 		return 1;
 	}
 
+	if (le32_to_cpu(raw_super->segment_count) > F2FS_MAX_SEGMENT) {
+		f2fs_msg(sb, KERN_INFO,
+			"Invalid segment count (%u)",
+			le32_to_cpu(raw_super->segment_count));
+		return 1;
+	}
+
 	/* check CP/SIT/NAT/SSA/MAIN_AREA area boundary */
 	if (sanity_check_area_boundary(sbi, bh))
 		return 1;
diff --git a/include/linux/f2fs_fs.h b/include/linux/f2fs_fs.h
index 4c02c6521fef..b7228c6b1e63 100644
--- a/include/linux/f2fs_fs.h
+++ b/include/linux/f2fs_fs.h
@@ -285,6 +285,12 @@ struct f2fs_nat_block {
 #define SIT_ENTRY_PER_BLOCK (PAGE_SIZE / sizeof(struct f2fs_sit_entry))
 
 /*
+ * F2FS uses 4 bytes to represent block address. As a result, supported size of
+ * disk is 16 TB and it equals to 16 * 1024 * 1024 / 2 segments.
+ */
+#define F2FS_MAX_SEGMENT       ((16 * 1024 * 1024) / 2)
+
+/*
  * Note that f2fs_sit_entry->vblocks has the following bit-field information.
  * [15:10] : allocation type such as CURSEG_XXXX_TYPE
  * [9:0] : valid block count
-- 
2.12.0

