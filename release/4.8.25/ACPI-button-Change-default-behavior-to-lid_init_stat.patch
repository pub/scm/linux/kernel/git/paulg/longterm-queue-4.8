From 9a19ece92934ea75926a26257917540556716f46 Mon Sep 17 00:00:00 2001
From: Lv Zheng <lv.zheng@intel.com>
Date: Thu, 12 Jan 2017 15:47:34 +0800
Subject: [PATCH] ACPI / button: Change default behavior to lid_init_state=open

commit 77e9a4aa9de10cc1418bf9a892366988802a8025 upstream.

More and more platforms need the button.lid_init_state=open quirk. This
patch sets it the default behavior.

If a platform doesn't send lid open event or lid open event is lost due to
the underlying system problems, then we can compare various combinations:
1. systemd/acpid is used to suspend system or not, systemd has a special
   logic forcing open event after resuming;
2. _LID returns a cached value or not.

The result is as follows:

 1. lid_init_state=method
   1. cached
      1. resumed by lid:
         (x) event=close
         (x) systemd=suspends again
         (x) acpid=suspends again
         (x) state=close
      2. resumed by other:
         (o) event=close
         (x) systemd=suspends again
         (x) acpid=suspends again
         (o) state=close
   2. non-cached
      1. resumed by lid:
         (o) event=open
         (o) systemd=resumes
         (o) acpid=resumes
         (o) state=open
      2. resumed by other:
         (o) event=close
         (x) systemd=suspends again
         (x) acpid=suspends again
         (o) state=close
 2. lid_init_state=open
   1. cached
      1. resumed by lid:
         (o) event=open
         (o) systemd=resumes
         (o) acpid=resumes
         (x) state=close
      2. resumed by other:
         (x) event=open
         (o) systemd=resumes
         (o) acpid=resumes
         (o) state=close
   2. non-cached
      1. resumed by lid:
         (o) event=open
         (o) systemd=resumes
         (o) acpid=resumes
         (o) state=open
      2. resumed by other:
         (x) event=open
         (o) systemd=resumes
         (o) acpid=resumes
         (o) state=close
 3. lid_init_state=ignore
   1. cached
      1. resumed by lid:
         (o) event=none
         (x) systemd=suspends again
         (o) acpid=resumes
         (x) state=close
      2. resumed by other:
         (o) event=none
         (x) systemd=suspends again
         (o) acpid=resumes
         (o) state=close
   2. non-cached
      1. resumed by lid:
         (o) event=none
         (x) systemd=suspends again
         (o) acpid=resumes
         (o) state=open
      2. resumed by other:
         (o) event=none
         (x) systemd=suspends again
         (o) acpid=resumes
         (o) state=close

As a conclusion:
 1. With systemd changed, lid_init_state=ignore has only one problem and the
    problem comes from an underlying issue, not userspace and kernel lid
    handling.
 2. Without systemd changed, lid_init_state=open can be the default
    behavior as the pass ratio is not much worse than lid_init_state=ignore.
 3. lid_init_state=method is buggy, we can have a separate patch to make it
    deprectated.

Link: https://bugzilla.kernel.org/show_bug.cgi?id=187271
Signed-off-by: Lv Zheng <lv.zheng@intel.com>
Signed-off-by: Rafael J. Wysocki <rafael.j.wysocki@intel.com>
Signed-off-by: Paul Gortmaker <paul.gortmaker@windriver.com>

diff --git a/drivers/acpi/button.c b/drivers/acpi/button.c
index 31abb0bdd4f2..0a4ff598c9c6 100644
--- a/drivers/acpi/button.c
+++ b/drivers/acpi/button.c
@@ -109,7 +109,7 @@ struct acpi_button {
 
 static BLOCKING_NOTIFIER_HEAD(acpi_lid_notifier);
 static struct acpi_device *lid_device;
-static u8 lid_init_state = ACPI_BUTTON_LID_INIT_METHOD;
+static u8 lid_init_state = ACPI_BUTTON_LID_INIT_OPEN;
 
 /* --------------------------------------------------------------------------
                               FS Interface (/proc)
-- 
2.12.0

